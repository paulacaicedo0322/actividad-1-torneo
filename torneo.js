// Aqui cree la conexion con mongo local 
var db = connect("localhost:27017/torneoDeportivo");

// aqui creamos las colecciones para el torneo 
db.createCollection("equipos");
db.createCollection("deportistas");
db.createCollection("entrenadores");
db.createCollection("arbitros");
db.createCollection("encuentrosDeportivos");
db.createCollection("resultados");
db.createCollection("posiciones");

    // Ingresamos los entrenadores 
    db.entrenadores.insertMany([
        {
            nombre: "Wilson Piedrahita",
            equipo: "Paisas de Antioquia",
            edad: 40
        },
        {
            nombre: "Caimanes de Barranquilla",
            equipo: "José Mosquera",
            edad: 45
        },
        {
            nombre: "Cimarrones del Chocó",
            equipo: "Plinio Rosero",
            edad: 38
        }
    ]);
  
    // Ingresamos los equipos 
    db.equipos.insertMany([
        {
            nombre: "Paisas de Antioquia",
            pais: "Colombia",
            ciudad: "Antioquia",
            entrenador: "Wilson Piedrahita"
        },
        {
            nombre: "Caimanes de Barranquilla",
            pais: "Colombia",
            ciudad: "Barranquilla",
            entrenador: "José Mosquera"
        },
        {
            nombre: "Cimarrones del Chocó",
            pais: "Colombia",
            ciudad: "Chocó",
            entrenador: "Plinio Rosero"
        }
    ]);
  
    // Ingresamos deportistas
    db.deportistas.insertMany([
        {
            nombre: "José Rodríguez",
            edad: 21,
            equipo: "Cimarrones del Chocó",
            posicion: "AP-P"
        },
        {
            nombre: "Rodrigo Caicedo",
            edad: 22,
            equipo: "Cimarrones del Chocó",
            posicion: "A"
        },
        {
            nombre: "Eleutherio Renteria",
            edad: 21,
            equipo: "Cimarrones del Chocó",
            posicion: "A"
        },
        {
            nombre: "Edgar Moreno",
            edad: 23,
            equipo: "Cimarrones del Chocó",
            posicion: "E-A"
        },
        {
            nombre: "Cesar Cortez",
            edad: 22,
            equipo: "Cimarrones del Chocó",
            posicion: "B-E"
        },
        {
            nombre: " Luis Escobar",
            edad: 19,
            equipo: "Caimanes de Barranquilla",
            posicion: "A"
        },
        {
            nombre: "Jeffry Niño",
            edad: 22,
            equipo: "Caimanes de Barranquilla",
            posicion: "B"
        },
        {
            nombre: "Ronald Ramírez",
            edad: 22,
            equipo: "Caimanes de Barranquilla",
            posicion: "A-P"
        },
        {
            nombre: "Jean Ruíz",
            edad: 24,
            equipo: "Caimanes de Barranquilla",
            posicion: "A"
        },
        {
            nombre: "Víctor Vargas",
            edad: 23,
            equipo: "Caimanes de Barranquilla",
            posicion: "B-E"
        },
        {
            nombre: "Marlon Piedrahíta",
            edad: 22,
            equipo: "Paisas de Antioquia",
            posicion: "B-E"
        },
        {
            nombre: "Sebastián Macías",
            edad: 21,
            equipo: "Paisas de Antioquia",
            posicion: "A"
        },
        {
            nombre: "Luis Tipton",
            edad: 23,
            equipo: "Paisas de Antioquia",
            posicion: "E-A"
        },
        {
            nombre: "Andrés Mosquera",
            edad: 23,
            equipo: "Paisas de Antioquia",
            posicion: "A"
        },
        {
            nombre: "uan Fernando Caicedo",
            edad: 20,
            equipo: "Paisas de Antioquia",
            posicion: "AP-P"
        },

    ]);

    // Ingresamos árbitros
    db.arbitros.insertMany([
        {
            nombre: "Calderón Rángel William",
            edad: 38,
            experiencia: "10 años"
        },
        {
            nombre: "Garcia Ferro, Carol Sacramento",
            edad: 39,
            experiencia: "8 años"
        }
    ]);

    // Ingresamos encuentros deportivos
    db.encuentrosDeportivos.insertMany([
        {
            arbitro: "Calderón Rángel William",
            equipoLocal: "Paisas de Antioquia",
            equipoVisitante: "Caimanes de Barranquilla",
            fecha: ISODate("2023-08-20T18:00:00Z"),
            lugar: "Estadio Antioquia"
        },
        {
            arbitro: "Garcia Ferro, Carol Sacramento",
            equipoLocal: "Caimanes de Barranquilla",
            equipoVisitante: "Cimarrones del Chocó",
            fecha: ISODate("2023-09-21T19:30:00Z"),
            lugar: "Estadio Bogotá"
        },
        {
            arbitro: "Calderón Rángel William",
            equipoVisitante: "Cimarrones del Chocó",
            fecha: ISODate("2023-10-20T18:00:00Z"),
            lugar: "Estadio Antioquia"
        },
        {
            arbitro: "Garcia Ferro, Carol Sacramento",
            equipoLocal: "Caimanes de Barranquilla",
            equipoVisitante: "Cimarrones del Chocó",
            fecha: ISODate("2023-11-20T18:00:00Z"),
            lugar: "Estadio Antioquia"
        }
    ]);

    // Ingresamos resultados
    db.resultados.insertMany([
        {
            encuentro: 1,
            equipoLocalPuntos: 72,
            equipoVisitantePuntos: 101
        },
        {
            encuentro: 2,
            equipoLocalPuntos: 103,
            equipoVisitantePuntos: 98
        },
        {
            encuentro: 3,
            equipoLocalPuntos: 78,
            equipoVisitantePuntos: 93
        },
        {
            encuentro: 4,
            equipoLocalPuntos: 82,
            equipoVisitantePuntos: 76
        }
    ]);

    // Ingresamos posiciones
    db.posiciones.insertMany([
        {
            equipo: "Caimanes de Barranquilla",
            puntos: 286,
            partidosJugados: 3,
            partidosGanados: 3,
            partidosEmpatados: 0,
            partidosPerdidos: 0,
            posicion:"Primero"
        },
        {
            equipo: "Cimarrones del Chocó",
            puntos: 267,
            partidosJugados: 3,
            partidosGanados: 1,
            partidosEmpatados: 0,
            partidosPerdidos: 2,
            posicion:"Segundo"
        },
        {
            equipo: "Paisas de Antioquia",
            puntos: 150,
            partidosJugados: 2,
            partidosGanados: 0,
            partidosEmpatados: 0,
            partidosPerdidos: 2,
            posicion:"Tercero"
        }
    ]);

print("Se creo correctamente la base de datos con sus respectivas colecciones, y documentos");
